#! /bin/bash

#### BY Earl Thibeault
### SIMPLE BASH SCRIPT CHECK YOUR ISO AGAINST FINGERPRINT AND MAKES BOOTABLE THUMB DRIVE
### works whith coreutils 8.2> ver of dd srry busybox 
ALGO=sha256sum
EXT=sha256
BSR=4609
until [ -d $1  ];do
##changes the hash if your gonna use this and try download do this first 
    if [ $1 = "hash" ] ; then
        case $2 in
            256) 
                ALGO=sha256sum
                EXT=sha256 ;;
            1)
                ALGO=sha1sum
                EXT=sha1 ;;
            5)
                ALGO=md5sum
                EXT=md5 ;;
            *)
                ALGO=sha256sum
                EXT=sha256
                echo "I don't know what you wanted but it's set to\
                sha256" ;;
            esac
        shift 2
##downloads iso to /tmp/bootscript ya still  gonna have to include dir and file name never been used as of writeing
    elif [ $1 = "down" ]; then
        mkdir /tmp/bootscript/
        cd /tmp/bootscript/
        wget $2&&
        wget $2.$EXT.txt
        shift 2
##changes the blocksize i like 4609 better thnen 512 idk
    elif [ $1 = "bs"]; then
        BSR=$2
        shift 2
    else
        echo "usage : ./bootscript.sh [hash, down, bs ] [(256,1,5) ,url to download, blocksize 
        ]\
         path_to_iso name_file.iso /dev/u_want_to_use"
    fi
done
##actul fingerprint check and dd ing also shows progress will wipe the drive your copppying to btw 
cd $1
if [ "$($ALGO $2)" = "$(cat $2.$EXT)" ]; then
    sudo mkfs.vfat -I $3
    sudo dd bs=$BSR if=$2 of=$3 status=progress
else
    echo "error your iso file was corrupted acording to checksum try again/
    maybe download the iso again "
fi
